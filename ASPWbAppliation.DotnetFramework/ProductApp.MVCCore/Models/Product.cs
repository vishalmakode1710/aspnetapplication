﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductApp.MVCCore.Models
{
    public class Product
    {
        #region Properties
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public bool isAvailable { get; set; } = true;
        public double? Price { get; set; } = 99.99;
        public string? Location { get; set; }
        #endregion
    }
}
