﻿
//6.0.6
using Microsoft.EntityFrameworkCore;
using ProductApp.MVCCore.Models;

namespace ProductApp.MVCCore.Context
{
    public class ProductDbContext:DbContext   //class
    {

        public ProductDbContext(DbContextOptions<ProductDbContext> options):base(options)
        {
            //Database.EnsureCreated();  //EnsureCreat Check DB is created/Not if Not then try to Create
        }

        //DB Table
        public DbSet<Product> Products { get; set; }
    }
}
