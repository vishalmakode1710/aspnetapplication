﻿using Microsoft.AspNetCore.Mvc;
using ProductApp.MVCCore.Exceptions;
using ProductApp.MVCCore.Models;
using ProductApp.MVCCore.Repository;
using ProductApp.MVCCore.Services;

namespace ProductApp.MVCCore.Controllers
{
    public class ProductController : Controller              //call Service layer
    {
        readonly IProductService _productService;
        public ProductController(IProductService productService )
        {
            _productService=productService;
        }
        //In controlller All the Function Named As Action
        // ActionResult is a class consist of multiple functionality
        //_productRepo is interface instance
        [HttpGet]
        public ActionResult Index()
        {
            List<Product>productList = _productService.GetProducts();
            return View(productList);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Product product)
        {
            try
            {
                int productAddResult = _productService.AddProduct(product);
                if (productAddResult == 1)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Redirect("Create");
                }
            }
            catch(ProductExistException pex)
            {
                return StatusCode(409, pex.Message);
            }           
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            Product? productDetails = _productService.GetProductById(id);
            if (productDetails != null)
            {
                return View(productDetails);
            }
            else
            {
                return View();
            }

        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            Product? productDetailsToDelete = _productService.DeleteProductById(id);
            return View(productDetailsToDelete);
        }

        [HttpPost]
        public ActionResult Delete(Product product)
        {
            int productDeletedResult = _productService.DeleteProduct(product.Id);
            if (productDeletedResult ==1)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }
    }
}
