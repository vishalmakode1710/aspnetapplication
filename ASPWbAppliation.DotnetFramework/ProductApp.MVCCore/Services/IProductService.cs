﻿using ProductApp.MVCCore.Models;

namespace ProductApp.MVCCore.Services
{
    public interface IProductService
    {
        List<Product> GetProducts();
        Product? DeleteProductById(int id);
        int DeleteProduct(int id);
        int AddProduct(Product product);
        Product? GetProductById(int id);
    }
}
