﻿using ProductApp.MVCCore.Exceptions;
using ProductApp.MVCCore.Models;
using ProductApp.MVCCore.Repository;

namespace ProductApp.MVCCore.Services
{
    public class ProductService : IProductService
    { 
        readonly IProductRepository _productRepository;
        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public int AddProduct(Product product)
        {
            Product? productDetailsExist = _productRepository.GetProductByName(product.Name);
            if(productDetailsExist == null)
            {
                return _productRepository.AddProduct(product);
            }
            else
            {
                throw new ProductExistException($"{product.Name} Alredy Exist");
            }
        }
        public int DeleteProduct(int id)
        {

            return _productRepository.DeleteProduct(id);
        }

        public Product? DeleteProductById(int id)
        {
            return _productRepository.DeleteProductById(id);
        }

        public Product? GetProductById(int id)
        {
            return _productRepository.GetProductById(id);
        }

        public List<Product> GetProducts()
        {
            return _productRepository.GetProducts();
        }
    }
}
