﻿using ProductApp.MVCCore.Models;

namespace ProductApp.MVCCore.Repository
{
    public interface IProductRepository  //functionality
    {
        List<Product> GetProducts();
        int AddProduct(Product product);
        int DeleteProduct(int id);
        Product? DeleteProductById(int id);
        Product? GetProductById(int id);
        Product? GetProductByName(string? name);
    }
}
