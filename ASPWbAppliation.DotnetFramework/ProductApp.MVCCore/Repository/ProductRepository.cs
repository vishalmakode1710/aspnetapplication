﻿using ProductApp.MVCCore.Models;
using ProductApp.MVCCore.Context;

namespace ProductApp.MVCCore.Repository
{
    public class ProductRepository : IProductRepository
    {
        ProductDbContext _productDbContext;
        public ProductRepository(ProductDbContext productDbContext)
        {
            _productDbContext = productDbContext;
        }

        public int AddProduct(Product product)
        {
            //if(_productDbContext.Products.Count() > 0)
            //{
            //    int maxId = _productDbContext.Products.Max(p => p.Id);     
            //    product.Id = maxId + 1;
            //}
            //else
            //{
            //    product.Id = 1;
            //}
            _productDbContext.Products.Add(product); //using table Products
            return _productDbContext.SaveChanges();
           
        }

        public int DeleteProduct(int id)
        {
            Product? productDetails = GetProductById(id);
            if (productDetails != null)
            {
                _productDbContext.Products.Remove(productDetails);
                return _productDbContext.SaveChanges();
            }
            else
            {
                return 0;
            }
        }

        public List<Product> GetProducts()
        {
            return _productDbContext.Products.ToList();
        }

        public Product? DeleteProductById(int id)
        {
            return _productDbContext.Products.Where(p => p.Id == id).FirstOrDefault();
        }

        public Product? GetProductById(int id)
        {
            return _productDbContext.Products.Where(p => p.Id == id).FirstOrDefault();
        }

        public Product? GetProductByName(string? name)
        {
            return _productDbContext.Products.Where(u => u.Name == name).FirstOrDefault();
        }
    }
}
