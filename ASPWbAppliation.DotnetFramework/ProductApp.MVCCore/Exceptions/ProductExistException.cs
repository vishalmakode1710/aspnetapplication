﻿namespace ProductApp.MVCCore.Exceptions
{
    public class ProductExistException:Exception
    {
        public ProductExistException()
        {

        }
        public ProductExistException(string message):base(message) //from exception class
        {

        }
    }
}
